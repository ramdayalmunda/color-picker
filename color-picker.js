var ColorPicker = function (configObj) {
    let defaultConfig = {
        containerId: "container",
        pickerMargin: 50,
        backgroundColor: "#aaaaaa",
    }
    let config = { ...defaultConfig, ...configObj }
    let container = document.getElementById(config.containerId)
    let canvas = document.createElement('canvas')
    canvas.height = (255 * 6) + config.pickerMargin * 2 + 400
    canvas.width = (255 * 6) + config.pickerMargin * 2
    canvas.style.width = '100%'
    container.append(canvas)

    init()

    function init() {

        setBackground()
        drawColorPickerBar()
    }

    function drawColorPickerBar() {


        const ctx = canvas.getContext('2d');
        ctx.save()
        // Draw a sample rectangle on the canvas
        let barWidth = 100;
        let perPixel = 1;
        let hPos = config.pickerMargin;
        let vPos = 255*6+config.pickerMargin*2
        // rgb(255,0,0) -> rgb(255,0,255)
        for (let i = 0; i < 255; i += perPixel) {
            ctx.fillStyle = `rgb(255, 0, ${i + 1})`;
            ctx.fillRect(i + hPos, vPos, perPixel, barWidth);
        }
        hPos += 255;
        // rgb(255,0,255) -> rgb(0,0,255)
        for (let i = 0; i < 255; i += perPixel) {
            ctx.fillStyle = `rgb(${255 - i}, 0, 255)`;
            ctx.fillRect(hPos + i, vPos, perPixel, barWidth);
        }
        hPos += 255;
        // rgb(0,0,255) -> rgb(0,255,255)
        for (let i = 0; i < 255; i += perPixel) {
            ctx.fillStyle = `rgb(0, ${i + 1}, 255)`;
            ctx.fillRect(hPos + i, vPos, perPixel, barWidth);
        }
        hPos += 255;
        // rgb(0,255,255) -> rgb(0,255,0)
        for (let i = 0; i < 255; i += perPixel) {
            ctx.fillStyle = `rgb(0, 255, ${255 - i})`;
            ctx.fillRect(hPos + i, vPos, perPixel, barWidth);
        }
        hPos += 255;
        // rgb(0,255,0) -> rgb(255,255,0)
        for (let i = 0; i < 255; i += perPixel) {
            ctx.fillStyle = `rgb(${i + 1}, 255, 0)`;
            ctx.fillRect(hPos + i, vPos, perPixel, barWidth);
        }
        hPos += 255;
        // rgb(255,255,0) -> rgb(255,0,0)
        for (let i = 0; i < 255; i += perPixel) {
            ctx.fillStyle = `rgb(255, ${255 - i}, 0)`;
            ctx.fillRect(hPos + i, vPos, perPixel, barWidth);
        }
        ctx.restore()


        ctx.save()
        let x = defaultConfig.pickerMargin;
        let y = 255*6 + defaultConfig.pickerMargin*2;
        let height = 100;
        let width = 255*6
        ctx.beginPath();
        ctx.fillStyle = config.backgroundColor;
        ctx.moveTo(x, y)
        ctx.arc(x+height/2, y+height/2, height/2, -Math.PI/2, Math.PI/2, true);
        ctx.lineTo(x,y+height/2*2)
        ctx.closePath();
        ctx.fill();
        
        ctx.beginPath()
        ctx.fillStyle = config.backgroundColor;
        ctx.moveTo(x+width, y)
        ctx.arc(x+width-height/2, y+height/2, height/2, -Math.PI/2, Math.PI/2, false);
        ctx.lineTo(x+width, y+height)
        ctx.closePath()

        ctx.fill();



        canvas.addEventListener('click', (e) => {
            let pos = getMousePos(e)
            const imageData = ctx.getImageData(pos.x, pos.y, 1, 1);
            const pixelData = imageData.data;
            const color = `rgb(${pixelData[0]}, ${pixelData[1]}, ${pixelData[2]})`;
            console.log(color);

        })
        ctx.restore()


    }

    function setBackground() {
        // add background
        const ctx = canvas.getContext('2d');
        ctx.save()

        ctx.fillStyle = defaultConfig.backgroundColor
        ctx.fillRect(0, 0, canvas.width, canvas.height)

        ctx.restore()
    }



    function getMousePos(evt) {
        var clx, cly
        if (evt.type == "touchstart" || evt.type == "touchmove") {
            clx = evt.touches[0].clientX;
            cly = evt.touches[0].clientY;
        } else {
            clx = evt.clientX;
            cly = evt.clientY;
        }
        var boundingRect = canvas.getBoundingClientRect();
        return {
            x: (clx - boundingRect.left) * canvas.width / canvas.clientWidth,
            y: (cly - boundingRect.top) * canvas.height / canvas.clientHeight
        };
    }

}